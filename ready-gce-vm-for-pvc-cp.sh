#!/bin/bash

SNAPSHOT_DATE=$1

if [ -z "$SNAPSHOT_DATE" ]; then
	echo 'Snapshot date must be specified as the first command.'
	exit 1
fi

apt-get update
apt-get install -y ssh

sed -i 's/PermitRootLogin no/PermitRootLogin yes/' -e /etc/ssh/sshd_config
systemctl restart ssh.service

mkdir ~/.ssh
cat >~/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCq4QKxY2Lmzq3o2rr1UbtL2G7Rn6TojEBfmcm0PuD6I+/ei70wkqN+VgRQUzjYarIXLlsbecAKrdkZfa948v5ZJaANlCyWHj4rGL6LGlGjXnEF7TyCw6dvN28l0r1NPbTeH+7iAi9aKJEBB19dgduggZQ81Ttk2Y9Q0WR+USL7VS2M5Tpo/do+P5hZgGH1YxWJ9/jonNRhOpnQmUbf3LpFaqIWtNolHoze6rfFU7y6koReXYrDn1xGn21ZBDb33at8VS6WAVaEIBpfgSr1sGehiC1sWThtRdxoYNcxWD/cX/Oq2rdxIdTwZ1/cMRfbDBMPJYaYz0bd1GsD68x9bmQD gcp@gitlab.freedesktop.org
EOF

for i in storage config-storage postgresql-storage redis-storage; do
	mkdir -p /mnt/fdo-gitlab-backup-${i}
	mount -o ro /dev/disk/by-id/google-fdo-gitlab-backup-${i}-${SNAPSHOT_DATE} /mnt/fdo-gitlab-backup-${i}
done

echo 'Configured as backup host for transfer.'
echo ''
echo 'Install the private SSH key into the ephemeral GKE guest, launch a shell, and run:'
echo '    # apt-get update && apt-get -y install ssh'
echo '    # for i in config-storage postgresql-storage redis-storage storage; do ssh root@GCE-VM-EXTERNAL-IP "cd /mnt/fdo-gitlab-backup-$i && tar c ." | tar -C /mnt/gitlab-$i -x; done'
