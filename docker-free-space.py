#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Author: Daniel Stone <daniel@fooishbar.org>
#
#
# This script monitors for free space on the /var/lib/docker mount, and
# sweeps up older images to free space where available.

import datetime
import functools
import os
import pprint
import sys
import time

import docker

def mib(bytes):
        return bytes * 1024 * 1024

def gib(bytes):
        return mib(bytes) * 1024

SLEEP_INTERVAL = 300

def is_protected_image(image):
        if any(image.tags, lambda t: t.starts_with("gitlab.freedesktop.org/mesa/mesa/debian")):
                return True
        if any(image.tags, lambda t: t.starts_with("gitlab.freedesktop.org/gstreamer/gst-ci")):
                return True
        return False

def is_enough_free(fs_size, bytes_avail):
        if fs_size >= gib(1024):
                return gib(150)
        else:
                return gib(40)

class FDOImage():
        def __init__(self, docker_server, docker_image):
                self.server = docker_server
                self.docker = docker_image
                self.id = self.docker["Id"]
                self.tags = self.docker["RepoTags"]
                self.is_running = False
                self.timestamp = None
        
        def __repr__(self):
                return "{!r}".format({ "id": self.id, "tags": self.tags, "is_running": self.is_running, "timestamp": self.timestamp })

        def ref(self, container):
                if not self.timestamp or container.timestamp > self.timestamp:
                        self.timestamp = container.timestamp
                if container.is_running:
                        self.is_running = True
                return self

        def should_cleanup(self):
                if self.is_running:
                        return False
                #if is_protected_image(self):
                        #return False
                return True


class FDOContainer():
        def __init__(self, server, docker_container):
                self.server = server
                self.docker = docker_container
                self.id = self.docker["Id"]
                self.is_running = (self.docker["State"] not in ["exited", "dead"])
                self.timestamp = datetime.datetime.fromtimestamp(self.docker["Created"])
                self.image = self.server.images[self.docker["ImageID"]].ref(self)

        def __repr__(self):
                return "{!r}".format({ "id": self.id, "image": self.image.id, "is_running": self.is_running, "time": self.timestamp })
        
        # clean up containers which exited more than an hour ago
        def should_cleanup(self):
                if self.is_running:
                        return False
                if self.timestamp >= datetime.datetime.now() - datetime.timedelta(hours=1):
                        return False
                return True


class FDOServer():
        def __init__(self, **kwargs):
                self.docker = docker.APIClient(kwargs)
                self.images = None
                self.containers = None

        def refresh_docker(self):
                self.images = { i["Id"]: FDOImage(self, i) for i in self.docker.images() }
                self.containers = { c["Id"]: FDOContainer(self, c) for c in self.docker.containers(all=True) }
                print("Images:")
                for i in self.images.values():
                        pprint.pprint(i, indent=4)
                print("Containers:")
                for c in self.containers.values():
                        pprint.pprint(c, indent=4)

        def should_cleanup(self):
                st = os.statvfs("/var/lib/docker")
                fs_size = st.f_bsize * st.f_blocks
                avail = st.f_bsize * st.f_bavail
                print("{avail} MB available from {fs_size} total".format(avail=avail/1024/1024, fs_size=fs_size/1024/1024))
                return not is_enough_free(fs_size, avail)

        def maybe_cleanup(self):
                self.refresh_docker()
                clean_containers = filter(lambda c: c.should_cleanup(), self.containers.values())
                for c in clean_containers:
                        print("clean C {}".format(c.id))
                clean_images = filter(lambda i: i.should_cleanup(), self.images.values())
                for i in clean_images:
                        print("clean I {} {}".format(i.id, i.timestamp))
                clean_images = sorted(clean_images)#, key=lambda i: i.timestamp, reverse=True)
                for i in clean_images:
                        print("sorted I {}".format(i.id))
                while True: #self.should_cleanup():
                        victim = clean_images.pop(0)
                        print("Removing {image_tags} (ID: {image_id})".format(image_tags=victim.tags, image_id=victim.id))
                        #return
                        #self.docker.remove(victim.id)
                self.images = None
                self.containers = None

if __name__ == '__main__':
        server = FDOServer()
        while True:
                server.maybe_cleanup()
                time.sleep(SLEEP_INTERVAL)
