#!/bin/sh

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
#  - $PACKET_DEVICE_NAME to the desired device name (e.g. fdo-packet-5)
#  - $GITLAB_RUNNER_REG_TOKEN to the shared-runner registration token from https://gitlab.freedesktop.org/admin/runners

packet device create --project-id $PACKET_PROJECT_ID --hostname $PACKET_DEVICE_NAME --plan m1.xlarge.x86 --facility ewr1 --operating-system debian_10 --userdata "$(sed -e "s/@@INSTANCENAME@@/$PACKET_DEVICE_NAME/; s/@@REGISTRATIONTOKEN@@/$GITLAB_RUNNER_REG_TOKEN/; s/@@CONCURRENT@@/8/" < gitlab-runner-cloud-init.yaml)"
